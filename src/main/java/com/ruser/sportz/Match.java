package com.ruser.sportz;

public class Match {
    Team winner;
    Team loser;
    public Match(Team winner, Team loser){
        this.winner = winner;
        this.loser = loser;
    }
    public Team getWinner(){
        return winner;
    }
    public Team getLoser(){
        return loser;
    }
}