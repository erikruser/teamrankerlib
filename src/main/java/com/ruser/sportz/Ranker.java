package com.ruser.sportz;

import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Ranker {
	
	private HashMap<String, Team> teams = new HashMap<String, Team>();
	private HashMap<Integer, Team> teamIndexes = new HashMap<Integer, Team>();
	private List<Team> teamList = new ArrayList<>();
	
	private RealMatrix colleyMatrix;
	private RealMatrix rankMatrix;

	private Boolean zeroMatchTeamsRatedZero = false;

	public Ranker(List<Team> teams){
	    teams.forEach(s -> addTeam(s));

		colleyMatrix = MatrixUtils.createRealMatrix(teams.size(), teams.size());
		rankMatrix = MatrixUtils.createRealMatrix(teams.size(), 1);
		for(int i = 0; i<teams.size(); i++){
			colleyMatrix.setEntry(i, i, 2);
			rankMatrix.setEntry(i,0,1);
		}
    }

	
	private void addTeam(Team team){
		int newIndex = teams.size(); //before put(), so teams will be zero indexed.
        teamList.add(team);
		teams.put(team.getCode(), team);
		team.setIndex(newIndex);
		teamIndexes.put(newIndex, team);
	}


	public void addMatch(Match match){

	    Team winningTeam = match.getWinner();
	    Team losingTeam = match.getLoser();

	    if(!teams.containsValue(winningTeam)){
	        throw new IllegalArgumentException("Team " + winningTeam.getName() + " is not a valid team.");
        }
        if(!teams.containsValue(losingTeam)){
	        throw new IllegalArgumentException("Team " + losingTeam.getName() + " is not a valid team.");
        }

		winningTeam.addWin();
		losingTeam.addLoss();
		
		int winnerIndex = winningTeam.getIndex();
		int loserIndex = losingTeam.getIndex();
		
		double gamesPlayed = colleyMatrix.getEntry(winnerIndex, loserIndex);
		colleyMatrix.setEntry(winnerIndex, loserIndex, gamesPlayed-1);
		colleyMatrix.setEntry(loserIndex, winnerIndex, gamesPlayed-1);
		double winnerTot = winningTeam.getWins() + winningTeam.getLosses() + 2;
		colleyMatrix.setEntry(winnerIndex, winnerIndex, winnerTot);
		double loserTot = losingTeam.getWins() + losingTeam.getLosses() + 2;
		colleyMatrix.setEntry(loserIndex, loserIndex, loserTot);
		
		double winnerRank = 1 + (Double.valueOf(winningTeam.getWins())-Double.valueOf(winningTeam.getLosses()))/2;
		rankMatrix.setEntry(winnerIndex, 0, winnerRank );
		double loserRank = 1 + (Double.valueOf(losingTeam.getWins())-Double.valueOf(losingTeam.getLosses()))/2;
		rankMatrix.setEntry(loserIndex, 0, loserRank );

	}


	private void computeRanking(){
		
		RealMatrix pInverse = new LUDecomposition(colleyMatrix).getSolver().getInverse();
		RealMatrix solution = pInverse.multiply(rankMatrix);
		
		double[] finalRanks = solution.getColumn(0);
		
		for(int i = 0; i<finalRanks.length; i++){
			teamIndexes.get(i).setRank(finalRanks[i]);
			if(
					zeroMatchTeamsRatedZero
					&& teamIndexes.get(i).getWins() == 0
					&& teamIndexes.get(i).getLosses() == 0
			){
				teamIndexes.get(i).setRank(0);
			}

		}

	}


	public List<Team> getRankedTeams(){
        computeRanking();
        Collections.sort(teamList);
	    return teamList;
    }

	public Team getTeam(String teamCode) {
		return teams.get(teamCode);
	}

	public Boolean getZeroMatchTeamsRatedZero() {
		return zeroMatchTeamsRatedZero;
	}

	public void setZeroMatchTeamsRatedZero(Boolean zeroMatchTeamsRatedZero) {
		this.zeroMatchTeamsRatedZero = zeroMatchTeamsRatedZero;
	}
}
