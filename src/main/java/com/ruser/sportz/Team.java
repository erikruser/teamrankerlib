package com.ruser.sportz;

import java.util.Arrays;

public class Team implements Comparable<Team>{
	
	private final String code;
	private final String name;
	private int index;
	private int wins = 0;
	private int losses = 0;
	private Double rank = 0.5;
	
	public int getWins() {
		return wins;
	}

	public void addWin() {
		this.wins++;
	}

	public int getLosses() {
		return losses;
	}

	public void addLoss() {
		this.losses++;
	}

	public Team(String code, String name){
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public double getRank() {
		return rank;
	}

	public void setRank(double rank) {
		this.rank = rank;
	}
	
	public double getPercent() {
		double winsd = wins;
		double lossesd = losses;
		return winsd / (winsd + lossesd);
	}

	public int compareTo(Team otherTeam) {
		return this.rank.compareTo(otherTeam.getRank());
	}

	@Override
	public int hashCode(){
		return Arrays.hashCode(code.toCharArray());
	}

	@Override
    public boolean equals(Object other){
        return this.code.equals( ((Team) other).getCode());
    }

}
