package com.ruser.sportz;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class RankerTest {

    private final double MAX_DELTA = 0.0001d;

    @Test
    public void testColleyMatrixRanker(){
        // Example team records and computed ratings from matrate.pdf example on page 12.

        Team a = new Team("a", "Team A");
        Team b = new Team("b", "Team B");
        Team c = new Team("c", "Team C");
        Team d = new Team("d", "Team D");
        Team e = new Team("e", "Team E");

        Ranker ranker = new Ranker(Arrays.asList(a, b, c, d, e));

        ranker.addMatch(new Match(a, c));
        ranker.addMatch(new Match(d, a));
        ranker.addMatch(new Match(e, a));
        ranker.addMatch(new Match(c, b));
        ranker.addMatch(new Match(b, e));
        ranker.addMatch(new Match(c, d));
        ranker.addMatch(new Match(e, c));

        List<Team> rankedTeams = ranker.getRankedTeams();

        HashMap<String, Team> rankedTeamsMap = new HashMap<>();
        rankedTeams.forEach(team -> rankedTeamsMap.put(team.getCode(), team));

        Assert.assertEquals(0.4130d, rankedTeamsMap.get("a").getRank(), MAX_DELTA);
        Assert.assertEquals(0.5217d, rankedTeamsMap.get("b").getRank(), MAX_DELTA);
        Assert.assertEquals(0.5000d, rankedTeamsMap.get("c").getRank(), MAX_DELTA);
        Assert.assertEquals(0.4783d, rankedTeamsMap.get("d").getRank(), MAX_DELTA);
        Assert.assertEquals(0.5870d, rankedTeamsMap.get("e").getRank(), MAX_DELTA);
    }

    @Test
    public void testColleyMatrixRanker_Trivial(){
        // Example team records and computed ratings from matrate.pdf example on page 12.

        Team a = new Team("a", "Team A");
        Team b = new Team("b", "Team B");
        Team c = new Team("c", "Team C");


        Ranker ranker = new Ranker(Arrays.asList(a, b, c));

        ranker.addMatch(new Match(a, b));

        List<Team> rankedTeams = ranker.getRankedTeams();

        HashMap<String, Team> rankedTeamsMap = new HashMap<>();
        rankedTeams.forEach(team -> rankedTeamsMap.put(team.getCode(), team));

        Assert.assertEquals(5d/8d, rankedTeamsMap.get("a").getRank(), MAX_DELTA);
        Assert.assertEquals(3d/8d, rankedTeamsMap.get("b").getRank(), MAX_DELTA);
        Assert.assertEquals(1d/2d, rankedTeamsMap.get("c").getRank(), MAX_DELTA);
    }

    @Test
    public void testColleyMatrixRanker_NoMatches(){
        Team a = new Team("a", "Team A");
        Team b = new Team("b", "Team B");
        Team c = new Team("c", "Team C");

        Ranker ranker = new Ranker(Arrays.asList(a, b, c));

        List<Team> rankedTeams = ranker.getRankedTeams();
        HashMap<String, Team> rankedTeamsMap = new HashMap<>();

        rankedTeams.forEach(team -> rankedTeamsMap.put(team.getCode(), team));

        Assert.assertEquals(1d/2d, rankedTeamsMap.get("a").getRank(), MAX_DELTA);
        Assert.assertEquals(1d/2d, rankedTeamsMap.get("b").getRank(), MAX_DELTA);
        Assert.assertEquals(1d/2d, rankedTeamsMap.get("c").getRank(), MAX_DELTA);
    }

}
