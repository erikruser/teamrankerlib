# Team Ranker Lib
Team Ranker Lib is a Java library implementing the Colley Matrix 
sports rating algorithm. The algorithm weights each team's win/loss 
record over a season based on a mathematical analysis of the team's 
strength of schedule.

## Usage
1. Team objects are created which represent each team in the league.
2. Game objects are created for each match. Each Game object stores which team is the winner, and which team is the loser. Ties are not supported by the algorithm.
3. All Team and Game objects are passed in to the Ranker object.
4. The Ranker object applies the Colley Matrix statistical analysis to the list of Teams, storing the computed rank on each Team object.

## Reference
Colley, W. N. (2002) *Colley’s Bias Free College Football Ranking Method:
The Colley Matrix Explained.* Retrieved from https://www.colleyrankings.com/matrate.pdf

## License
[Mozilla Public License 2.0](https://choosealicense.com/licenses/mpl-2.0/)